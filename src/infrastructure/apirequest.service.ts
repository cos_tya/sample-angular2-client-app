import {Injectable} from "@angular/core";
import {Http} from "@angular/http";

@Injectable()
export class ApiRequestService {

  constructor(private http: Http) {
  }

  private makeCall(url: string): Promise<any> {
    return new Promise(resolve => {
      this.http.get(url)
        .subscribe(response => {
          let body = response.json() || null;
          resolve(body);
        }, err => {
          console.error(err);
          resolve(null);
        });
    });
  }

  getOne(url: string): Promise<{}> {
    return this.makeCall(url)
      .then(response => {
        let result = response || {};
        return Promise.resolve(result);
      });
  }

  getMultiple(url: string): Promise<Array<{}>> {
    return this.makeCall(url)
      .then(response => {
        let results = [];
        if (response) {
          results = response.results;
        }
        return Promise.resolve(results);
      })
  }
}
