import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {HttpModule} from "@angular/http";
import {CommonModule} from "../infrastructure/common.module";
import {PlanetComponent} from "./planet.component";

@NgModule({
  imports: [BrowserModule, HttpModule, CommonModule],
  declarations: [PlanetComponent],
  exports: [PlanetComponent]
})
export class PlanetModule {
}
