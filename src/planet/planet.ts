
export class Planet {
  name: string;
  climate: string;
  terrain: string;
}
