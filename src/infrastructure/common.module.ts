import {NgModule} from "@angular/core";
import {ApiRequestService} from "./apirequest.service";
import {HttpModule} from "@angular/http";

@NgModule({
  imports: [HttpModule],
  providers: [ApiRequestService]
})
export class CommonModule {}
