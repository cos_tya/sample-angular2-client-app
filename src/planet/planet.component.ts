import {Component, Input, OnChanges, SimpleChanges} from "@angular/core";
import {Planet} from "./planet";
import {PlanetService} from "./planet.service";

@Component({
  selector: 'homeworld',
  templateUrl: './planet/planet.component.html',
  providers: [PlanetService]
})
export class PlanetComponent implements OnChanges{

  @Input() reference: string;
  planet: Planet;

  constructor(private planetService: PlanetService){
  }

  ngOnChanges(changes: SimpleChanges): void {
    let referenceChange = changes['reference'];
    if(referenceChange.currentValue != referenceChange.previousValue) {
      this.planetService
        .getPlanet(referenceChange.currentValue)
        .then(planet=> this.planet=planet);
    }
  }
}
