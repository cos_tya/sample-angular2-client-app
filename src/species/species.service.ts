import {Injectable} from "@angular/core";
import {Species} from "./species";
import {ApiRequestService} from "../infrastructure/apirequest.service";

@Injectable()
export class SpeciesService {

  constructor(private apiRequest: ApiRequestService){
  }

  getSpecies(references: string[]): Promise<Array<Species>> {
    let promises = references.map(reference=> this.apiRequest.getOne(reference));
    return Promise.all(promises);
  }
}
