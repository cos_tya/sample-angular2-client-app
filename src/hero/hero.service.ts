import {Injectable} from "@angular/core";
import {Hero} from "./hero";
import {ApiRequestService} from "../infrastructure/apirequest.service";
import {Observable, Subject} from "rxjs/Rx";

// Observable operators
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/combineLatest';
import 'rxjs/add/observable/merge';


export class Paging{
  nextRef:string = null;
  previousRef: string = null;
}


@Injectable()
export class HeroService {
  private urlSubject: Subject<string>;
  private initSearch: Subject<Array<Hero>>;
  private heroes: Observable<Hero[]>;
  private nextPageSubject: Subject<string>;
  private paging: Paging;

  constructor(private apiRequest: ApiRequestService) {
    this.urlSubject = new Subject<string>();
    this.initSearch = new Subject<Array<Hero>>();
    this.nextPageSubject = new Subject<string>();
    this.initStrings();
    this.setPagination();
    this.initSearch.next([]);
  }

  private getUrl(suffix?: string) {
    suffix = suffix || '';
    return 'http://swapi.co/api/people/' + suffix;
  }

  private initStrings(): void {
    let heroesSourceString = this
      .nextPageSubject.startWith(this.getUrl())
      .switchMap(url=> Observable.fromPromise(this.apiRequest.getOne(url)));

    let foundHeroes = this.urlSubject
      .debounceTime(300)
      .distinctUntilChanged()
      .switchMap(url=> Observable.fromPromise(this.apiRequest.getMultiple(url)))
      .startWith([])
      .merge(this.initSearch);

    this.heroes = heroesSourceString
      .combineLatest(foundHeroes, (heroesSource, foundHeroes)=> {
        if (foundHeroes.length === 0) {
          this.setPagination(heroesSource);
          return this.fetchHeroes(heroesSource);
        }
        this.setPagination();
        return foundHeroes;
      });
  }

  private setPagination(source?: any) {
    if (source) {
      this.paging.nextRef = source['next'];
      this.paging.previousRef = source['previous'];
    } else {
      this.paging = this.paging || new Paging();
      this.paging.nextRef = null;
      this.paging.previousRef = null;
    }
  }

  private fetchHeroes(heroesSource: any): Hero[] {
    return heroesSource['results'];
  }

  getAllHeroes(): Observable<Hero[]> {
    return this.heroes;
  }

  searchHeroes(term: string): void {
    if (term) {
      let suffix = '?search=' + term;
      this.urlSubject.next(this.getUrl(suffix));
    } else {
      this.initSearch.next([]);
    }
  }

  getPaging(): Paging {
    return this.paging;
  }

  goPage(ref: string): void {
    this.nextPageSubject.next(ref);
  }
}
