import {NgModule}      from '@angular/core';
import {HeroListComponent} from "./hero-list.component";
import {BrowserModule} from "@angular/platform-browser";
import {CommonModule} from "../infrastructure/common.module";
import {SpeciesModule} from "../species/species.module";
import {PlanetModule} from "../planet/palnet.module";
import {HeroService} from "./hero.service";
import {HeroSearchComponent} from "./hero-search.component";
import {ModalModule} from "ngx-bootstrap";


@NgModule({
  imports: [BrowserModule, CommonModule, SpeciesModule, PlanetModule, ModalModule.forRoot()],
  declarations: [HeroListComponent, HeroSearchComponent],
  exports: [HeroListComponent, HeroSearchComponent],
  providers: [HeroService]
})
export class HeroModule {
}
