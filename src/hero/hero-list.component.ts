import {Component, OnInit} from "@angular/core";
import {Hero} from "./hero";
import {HeroService, Paging} from "./hero.service";
import {Observable} from "rxjs";

@Component({
  selector: 'hero-list',
  templateUrl: './hero/hero-list.component.html'
})
export class HeroListComponent implements OnInit{
  heroes: Observable<Hero[]>;
  selectedHero: Hero;
  paging: Paging;

  constructor(private heroService: HeroService){}

  ngOnInit(): void {
    this.heroes = this.heroService.getAllHeroes();
    this.paging = this.heroService.getPaging();
  }

  showHero(hero: Hero, modal: any): void {
    this.selectedHero = hero;
    modal.show();
  }

  goPage(ref: string): void {
    this.heroService.goPage(ref);
  }
}
