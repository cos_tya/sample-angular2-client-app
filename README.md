# Test Client Application

This is a prototype Client Web Application written with Angular2, Bootstrap.
It shows characters from Star Wars series.

## Functionality:

* it shows heroes of 'Star Wars'
* list of heroes has 'prev' an 'next' pagination button
* search field allows to search for heroes by their name
* if search does not find any hero the whole list of heroes is shown

## to run app
    $ npm install
    $ npm start
  
this lunches `lite-server` and opens default browser on `localhost:3000`
