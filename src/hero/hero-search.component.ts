import {Component} from "@angular/core";
import {HeroService} from "./hero.service";

@Component({
  selector: 'hero-search',
  templateUrl: './hero/hero-search.component.html'
})
export class HeroSearchComponent{
  constructor(private heroService: HeroService){
  }

  search(term: string): void {
    this.heroService.searchHeroes(term);
  }
}
