import {NgModule} from "@angular/core";
import {CommonModule as AppCommonModule} from "../infrastructure/common.module";
import {SpeciesListComponent} from "./species-list.component";
import {BrowserModule} from "@angular/platform-browser";

@NgModule({
  imports: [BrowserModule, AppCommonModule],
  declarations: [SpeciesListComponent],
  exports: [SpeciesListComponent]
})
export class SpeciesModule {
}
