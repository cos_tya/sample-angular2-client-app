import {Injectable} from "@angular/core";
import {ApiRequestService} from "../infrastructure/apirequest.service";
import {Planet} from "./planet";

@Injectable()
export class PlanetService {
  constructor(private apiRequest: ApiRequestService){
  }

  getPlanet(reference: string): Promise<Planet> {
    return this.apiRequest.getOne(reference);
  }
}
