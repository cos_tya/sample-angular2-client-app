import {Component, Input, OnChanges, SimpleChanges} from "@angular/core";
import {Species} from "./species";
import {SpeciesService} from "./species.service";

@Component({
  selector: 'species-list',
  templateUrl: './species/species-list.component.html',
  providers: [SpeciesService]
})
export class SpeciesListComponent implements OnChanges {
  @Input() references: string[];
  speciess: Species[];

  constructor(private speciesService: SpeciesService) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('references')
      && changes['references'].currentValue != changes['references'].previousValue) {
      this.speciesService
        .getSpecies(changes['references'].currentValue)
        .then(species=> this.speciess = species);
    }
  }
}
